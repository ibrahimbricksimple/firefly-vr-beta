using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class Menu : MonoBehaviour {
  private Vector3 startingPosition;

  void Start() {
    startingPosition = transform.localPosition;
    SetGazedAt(false);
  }

 

  public void SetGazedAt(bool gazedAt) {
    GetComponent<Renderer>().material.color = gazedAt ? Color.green : Color.red;
  }

  public void Reset() {
    transform.localPosition = startingPosition;
  }



  public void TeleportRandomly() {
    Vector3 direction = Random.onUnitSphere;
    direction.y = Mathf.Clamp(direction.y, 0.5f, 1f);
    float distance = 2 * Random.value + 1.5f;
    transform.localPosition = direction * distance;
  }

  #region IGvrGazeResponder implementation

  /// Called when the user is looking on a GameObject with this script,
  /// as long as it is set to an appropriate layer (see GvrGaze).
  public void OnGazeEnter() {
		SetGazedAt(true);
		Debug.Log ("Roll");
		AudioSource audio = GameObject.Find("rollover-tone").GetComponent<AudioSource>();
		audio.Play();
  }

  /// Called when the user stops looking on the GameObject, after OnGazeEnter
  /// was already called.
  public void OnGazeExit() {
    SetGazedAt(false);
  }

  /// Called when the viewer's trigger is used, between OnGazeEnter and OnGazeExit.
  public void OnGazeTrigger() {
		Debug.Log ("Select");
		AudioSource audio = GameObject.Find("select").GetComponent<AudioSource>();
		audio.Play();
  }



  #endregion
}
