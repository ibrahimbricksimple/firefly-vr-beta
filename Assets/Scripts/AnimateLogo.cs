﻿using UnityEngine;
using UnityEngine.Assertions;

public class AnimateLogo : MonoBehaviour {
	
	[SerializeField]
	private Transform frog, letters;

	[SerializeField]
	private Quaternion startRotaion, endRotation;

	[SerializeField]
	private float rotationTime;

	[SerializeField]
	private float lingerTime;
	private float lingerPercent;

	[SerializeField]
	[Range(0,1)]
	private float rotationPercent = 0;

	public bool isLooping = true;

	public AnimationCurve curve;

	void Start () {
		Assert.raiseExceptions = true;
		Assert.IsNotNull (frog);
		Assert.IsNotNull (letters);

	}

	void Update () {
		if ((lingerPercent += Time.deltaTime / lingerTime) < 1)
			return;
		
		//update rotation percent
		if ((rotationPercent += Time.deltaTime / rotationTime) > 1)
			if (isLooping){
				rotationPercent = 0;
				lingerPercent = 0;
			}
			else 
				return;
		updateTransforms ();
	}

	//allow scrubbbing through the animation in the editor by updating the transforms everytime the editor is changed
	void OnValidate(){
		updateTransforms ();
	}

	private void updateTransforms(){
		//apply easing function on time 
		//float easedPercent = (float)Easing.CubicEaseOut (rotationPercent, 0, 1, 1);
		float easedPercent = curve.Evaluate(rotationPercent);
			
		//scale the frog up and rotate everything
		frog.localScale = new Vector3 (easedPercent, easedPercent, easedPercent);
		letters.localRotation = Quaternion.Slerp (startRotaion, endRotation, easedPercent);
	}
}
