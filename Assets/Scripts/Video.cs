﻿using UnityEngine;
using System.Collections;

public class Video : MonoBehaviour {

	public string url;
	public string targetScene;
	public int rotateScene;

	public void LoadScene(PersistentVideoData videoData) {

		// Commented out since variables would not update when selecting different menu items (Ibrahim@bricksimple.com)
		//videoData.url = url;
		//video	Data.rotateScene = rotateScene;

		//GameObject.Find ("PersistentVideoData").GetComponent<PersistentVideoData> ().url = url;

		GameObject.Find ("PersistentVideoData").GetComponent<PersistentVideoData> ().rotateScene = (int)GameObject.Find ("Main Camera").transform.eulerAngles.y;

		Debug.Log ("Main Camera Rotation: " + GameObject.Find ("Main Camera").transform.eulerAngles.y);

		SceneFader.Get().FadeToScene(targetScene);
	
	}
}
