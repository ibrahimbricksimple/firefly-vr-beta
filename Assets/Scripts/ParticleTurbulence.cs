﻿using UnityEngine;

[RequireComponent(typeof (ParticleSystem))]
public class ParticleTurbulence : MonoBehaviour {
	
	#region LocalVariables
	[Header("Flight Path Randomness")]
	[Tooltip("How much influence the Perlin noise will have on the particles")]
	[Range(0.0f, 10.0f)]
	public float randomStrength = 1.0f;
	[Tooltip("How quickly the Perlin noise changes")]
	[Range(0.0f, 10.0f)]
	public float randomStepModifier = 1.0f; 

	#region SimplexParameters
	[Tooltip("The inverse factor for initial speed (ex. 0.5 will double init speed)")]
	public float speedFactor = 0.3f;

	[Tooltip("the percent of curl added to the particles' velocity each frame\n0 would be no curl and 1 would make the particles follow the flow lines exactly")]
	[Range(0f,1f)]
	public float curlFactor = .07f;

	[Tooltip("defines how quickly the perlin field should step")]
	public float morphSpeed;

	[Tooltip("defines the strength of the amplitude (is the amplitude if damping is false)")]
	[Range(0f, 1f)]
	public float strength = .215f;

	[Tooltip("Provides simple smoothing for the noise")]
	public bool damping;

	[Tooltip("How quickly adjacent noise values change value")]
	public float frequency = 1f;

	[Tooltip("define the octaves for the perlin noise which defines how many 'layers' of noise to overlay together\nHigher values increase resolution/quality but is very very expensive")]
	[Range(1, 8)]
	public int octaves = 1;

	[Tooltip("how quickly the frequency increases in each successive octave")]
	[Range(1f, 4f)]
	public float lacunarity = 2f;

	[Tooltip("how quickly the amplitude decreases in each successive octave")]
	[Range(0f, 1f)]
	public float persistence = 0.5f;

	//will be used in advancing the perlin field
	private float morphOffset;
	#endregion


	private ParticleSystem				particleSystem;
	private ParticleSystem.Particle[]	particleArray;

	#endregion

	void Start () {
		particleSystem	=	GetComponent<ParticleSystem>();
		particleArray = new ParticleSystem.Particle[GetComponent<ParticleSystem>().maxParticles];
	}

	void LateUpdate () {
		int particleCount = particleSystem.GetParticles(particleArray);
		PositionParticles ();
		particleSystem.SetParticles(particleArray,particleCount); 
	}

	//Note(Ryan): should mod be used instead of subtraction?
	void UpdateMorph(){
		//increment the morph value based on time elapsed and the morph speed
		morphOffset += Time.deltaTime * morphSpeed;

		//limit morph to 256 since the hashmap is only 256 ints long
		if (morphOffset > 256f) {
			morphOffset -= 256f;
		}
	}

	/*This method handles the turbulence type effect that I want to induce on the initial
 * particle system using 3D perlin noise. Unfortunately, Unity does not have 3D perlin
 * noise built in (only the 2D mathf.PerlineNoise()) so I had to include the following
 * math created by Ken Perlin.
 * It relies on the NoiseSample struct defined in noise.cs as well 
 */
	private void PositionParticles()
    {
        //base the perlin on the location of the explosion gameObject
        Quaternion q = Quaternion.Euler(this.transform.position);
        Quaternion qInv = Quaternion.Inverse(q);

        //set the amplitude based on whether damping is on or not
        float amplitude = damping ? strength / frequency : strength;

        //increment the morph value based on time elapsed and the morph speed
        morphOffset += Time.deltaTime * morphSpeed;

        //limit morph to 256 since the hashmap is only 256 ints long
        if (morphOffset > 256f)
        {
            morphOffset -= 256f;
        }
        //looping through every particle
        for (int i = 0; i < particleArray.Length; i++)
        {
            //store the current particle's position
            Vector3 position = particleArray[i].position;
            //define a point based on current location
            Vector3 point = q * new Vector3(position.z, position.y, position.x + morphOffset);
            //lookup what the perlin value is at that point in the field
            NoiseSample sampleX = Noise.Sum(point, frequency, octaves, lacunarity, persistence);
            //scale the sample value by the amplitude (to more/less than the 0 to 1 range)
            sampleX *= amplitude;
            sampleX.derivative = qInv * sampleX.derivative;

            //repeat above with sampleY
            point = q * new Vector3(position.x + 100f, position.z, position.y + morphOffset);
            NoiseSample sampleY = Noise.Sum(point, frequency, octaves, lacunarity, persistence);
            sampleY *= amplitude;
            sampleY.derivative = qInv * sampleY.derivative;

            //repeat above with sampleZ
            point = q * new Vector3(position.y, position.x + 100f, position.z + morphOffset);
            NoiseSample sampleZ = Noise.Sum(point, frequency, octaves, lacunarity, persistence);
            sampleZ *= amplitude;
            sampleZ.derivative = qInv * sampleZ.derivative;

            //define the curl vector 
            Vector3 curl;
            //calculate the x, y, z components of the curl vector using the normal vectors at each
            //of the points in the field
            curl.x = sampleZ.derivative.x - sampleY.derivative.y;
            curl.y = sampleX.derivative.x - sampleZ.derivative.y + (1f / (1f + position.y));
            curl.z = sampleY.derivative.x - sampleX.derivative.y;

            //adjust the current velocity of the particle based on the curlFactor percent
            particleArray[i].velocity = particleArray[i].velocity * (1f - curlFactor) + curl * curlFactor;
        }
    }
}

