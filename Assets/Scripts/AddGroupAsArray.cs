﻿using UnityEngine;

public class AddGroupAsArray : MonoBehaviour {

	[SerializeField]
	protected Transform[] array;

	[SerializeField]
	protected bool flattenOnce, flattenAll;

	void OnValidate(){
		if (!flattenOnce && ! flattenAll) 
			return;
		
		if(flattenOnce)
			array.FlattenOnce (ref array);
		else 
			array.FlattenAll (ref array);
		flattenOnce = false;
		flattenAll = false;
	}
}