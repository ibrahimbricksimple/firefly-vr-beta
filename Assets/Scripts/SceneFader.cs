using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneFader : MonoBehaviour {

	private static readonly string label = "SceneFader";
	
	public float fadeDuration;
	public Color fadeColor = Color.white;

	private float alpha = 0;
	//keeps track of whether we are fading in or out
	private int fadeDirection;
	//whether or not to fade
	private bool fading = false;
	private string nextScene;
	private Rect screenRect;

	void Awake() {
		DontDestroyOnLoad(gameObject);

		if(FindObjectsOfType(GetType()).Length > 1) {
			Destroy(gameObject);
		}
	}

	void Start() {
		screenRect = new Rect(0, 0, Screen.width, Screen.height);
		name = label;
	}

	void OnLevelWasLoaded() {
		//adjust to new screen size if neccessary
		screenRect = new Rect(0, 0, Screen.width, Screen.height);
	}

	void OnGUI() {
		if(fading) {
			//adjust alpha if fading
			alpha += (Time.deltaTime / fadeDuration) * fadeDirection;

			//if alpha is beyond bounds then reset direction and load scene or stop fading
			if(alpha >= 1) {
				fadeDirection = -1;
				SceneManager.LoadScene(nextScene);
			}

			if(alpha <= 0) {
				fadeDirection = 1;
				fading = false;
			}
		}

		//update the GUI overlay
		GUI.color = new Color(fadeColor.r, fadeColor.g, fadeColor.b, alpha);
		GUI.depth = -1000; // Draw in front of everything else
		GUI.DrawTexture(screenRect, Texture2D.whiteTexture);
	}

	//set up all of the variables needed to start fading to a scene
	public void FadeToScene(string scene) {
		if(!fading) {
			alpha = 0;
			fading = true;
			fadeDirection = 1;
			nextScene = scene;
		}
	}

	public static SceneFader Get() {
		GameObject found = GameObject.Find(label);
		if(found != null) return found.GetComponent<SceneFader>();
		return null;
	}
}