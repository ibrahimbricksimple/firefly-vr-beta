﻿using System;
using System.Collections.Generic;

public class ListChain<T> : List<T> {
	new public ListChain<T> Add(T a){
		base.Add (a);
		return this;
	}
	new public ListChain<T> AddRange(IEnumerable<T> collection){
		base.AddRange(collection);
		return this;
	}
	new public ListChain<T> Clear(){
		base.Clear ();
		return this;
	}
	new public ListChain<T> Insert(int index, T item){
		base.Insert (index, item);
		return this;
	}
	new public ListChain<T> InsertRange(int index, IEnumerable<T> collection){
		base.InsertRange (index, collection);
		return this;
	}
	new public ListChain<T> RemoveAt(int index){
		base.RemoveAt (index);
		return this;
	}
	new public ListChain<T> RemoveRange(int index, int count){
		base.RemoveRange (index, count);
		return this;
	}
	new public ListChain<T> Reverse(){
		base.Reverse ();
		return this;
	}
	new public ListChain<T> Reverse(int index, int count){
		base.Reverse (index, count);
		return this;
	}
	new public ListChain<T> Sort(){
		base.Sort ();
		return this;
	}
	new public ListChain<T> Sort(Comparison<T> comparison){
		base.Sort (comparison);
		return this;
	}
	new public ListChain<T> Sort(IComparer<T> comparison){
		base.Sort (comparison);
		return this;
	}
	new public ListChain<T> TrimExcess(){
		base.TrimExcess ();
		return this;
	}
}

