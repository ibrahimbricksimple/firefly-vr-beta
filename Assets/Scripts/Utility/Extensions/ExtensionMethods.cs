﻿using UnityEngine;
using System.Linq;
using System;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

public static class ExtensionMethods {
	#region FlattenLists
	public static void FlattenOnce(this ListChain<Transform> list){
		ListChain<Transform> tempList = new ListChain<Transform> ();
		int index = 0;
		foreach (Transform t in list) {
			if (t.childCount > 0)
				for(int subIndex = 0; subIndex < t.childCount; subIndex++)	
					tempList.Add (t.GetChild (subIndex));
			else
				tempList.Add (t);
			index++;
		}
		list.Clear().AddRange(tempList);
	}

	public static void FlattenOnce(this Transform[] array, ref Transform[] referenceArray){
		ListChain<Transform> tempList = new ListChain<Transform> ();
		tempList.AddRange(array).FlattenOnce ();
		referenceArray = new Transform[tempList.Count];
		Array.Copy (tempList.Cast<Transform> ().ToArray (), referenceArray, tempList.Count);
	}

	public static void FlattenAll(this ListChain<Transform> list){
		int size = list.Count;
		int newSize = 0;
		while (newSize != size) {
			list.FlattenOnce ();
			size = newSize;
			newSize = list.Count;
		}
	}

	public static void FlattenAll(this Transform[] array, ref Transform[] referenceArray){
		int size = referenceArray.Count();
		int newSize = 0;
		while (newSize != size) {
			referenceArray.FlattenOnce (ref referenceArray);
			size = newSize;
			newSize = referenceArray.Count();
		}
	}
	#endregion

	#region Primitives
	/// <summary>
	/// Map the specified value, in the range (from1, to1) to a value between from2 and to2.
	/// </summary>
	/// <param name="value">The value in question.</param>
	/// <param name="from1">The first lower bound.</param>
	/// <param name="to1">The first higher bound.</param>
	/// <param name="from2">The second lower bound.</param>
	/// <param name="to2">The second higher bound.</param>
	public static float Map(this float value, float from1, float to1, float from2, float to2) {
		return from2 + ((to2 - from2) * ((value - from1) / (to1 - from1)));
	}

	public static int WithRandomSign(this int value, float negativeProbability){
		return UnityEngine.Random.value < negativeProbability ? -value : value;
	}

	public static float WithRandomSign(this float value, float negativeProbability){
		return UnityEngine.Random.value < negativeProbability ? -value : value;
	}

	public static float Clamp01(this float value){
		return Mathf.Clamp01 (value);
	}

	#endregion

	#region Strings
	public static string Truncate(this string value, int maxLength){
		if(string.IsNullOrEmpty(value)) return value;
		return value.Length <= maxLength ? value : value.Substring (0, maxLength);
	}

	// Named format strings from object attributes. Eg:
    // string blaStr = aPerson.ToString("My name is {FirstName} {LastName}.")
    // From: http://www.hanselman.com/blog/CommentView.aspx?guid=fde45b51-9d12-46fd-b877-da6172fe1791
    public static string ToString(this object anObject, string aFormat) {
        return ToString(anObject, aFormat, null);
    }

    public static string ToString(this object anObject, string aFormat, IFormatProvider formatProvider) {
        StringBuilder sb = new StringBuilder();
        Type type = anObject.GetType();
        Regex reg = new Regex(@"({)([^}]+)(})", RegexOptions.IgnoreCase);
        MatchCollection mc = reg.Matches(aFormat);
        int startIndex = 0;
        foreach (Match m in mc)
        {
            Group g = m.Groups[2]; //it's second in the match between { and }
            int length = g.Index - startIndex - 1;
            sb.Append(aFormat.Substring(startIndex, length));

            string toGet = string.Empty;
            string toFormat = string.Empty;
            int formatIndex = g.Value.IndexOf(":"); //formatting would be to the right of a :
            if (formatIndex == -1) //no formatting, no worries
            {
                toGet = g.Value;
            }
            else //pickup the formatting
            {
                toGet = g.Value.Substring(0, formatIndex);
                toFormat = g.Value.Substring(formatIndex + 1);
            }

            //first try properties
            PropertyInfo retrievedProperty = type.GetProperty(toGet);
            Type retrievedType = null;
            object retrievedObject = null;
            if (retrievedProperty != null)
            {
                retrievedType = retrievedProperty.PropertyType;
                retrievedObject = retrievedProperty.GetValue(anObject, null);
            }
            else //try fields
            {
                FieldInfo retrievedField = type.GetField(toGet);
                if (retrievedField != null)
                {
                    retrievedType = retrievedField.FieldType;
                    retrievedObject = retrievedField.GetValue(anObject);
                }
            }

            if (retrievedType != null) //Cool, we found something
            {
                string result = string.Empty;
                if (toFormat == string.Empty) //no format info
                {
                    result = retrievedType.InvokeMember("ToString",
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.IgnoreCase
                        , null, retrievedObject, null) as string;
                }
                else //format info
                {
                    result = retrievedType.InvokeMember("ToString",
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.IgnoreCase
                        , null, retrievedObject, new object[] { toFormat, formatProvider }) as string;
                }
                sb.Append(result);
            }
            else //didn't find a property with that name, so be gracious and put it back
            {
                sb.Append("{");
                sb.Append(g.Value);
                sb.Append("}");
            }
            startIndex = g.Index + g.Length + 1;
        }
        if (startIndex < aFormat.Length) //include the rest (end) of the string
        {
            sb.Append(aFormat.Substring(startIndex));
        }
        return sb.ToString();
    }
	#endregion
}