﻿using UnityEngine;

public class PointAtMe : MonoBehaviour {
	public Transform[] onLookers;
	public bool point;

	public void Update(){
		for (int i = 0; i < onLookers.Length; i++) 
			Debug.DrawLine (transform.position, onLookers [i].position, Color.red);
	}

	void OnValidate(){
		for (int i = 0; i < onLookers.Length; i++) 
			onLookers [i].LookAt (transform, Vector3.up);
	}
}