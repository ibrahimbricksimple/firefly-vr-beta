﻿using UnityEngine;

public class SpriteAnimation : MonoBehaviour {

	[Tooltip ("How many frames this animation has.")]
	public int frames;

	[Tooltip ("How long the entire animation lasts.")]
	public float duration;

	[Tooltip ("The material used to offset the texture to use the correct sprite.")]
	public Material mat;

	private Vector2 textureOffset;
	private float textureStep;
	private float timePerFrame;
	private float timeElapsed;

	void Start() {
		textureStep = 1F / frames;
		timePerFrame = duration / frames;
		textureOffset = new Vector2();
	}

	void Update() {
		int frameNumber = (int) (timeElapsed / duration) % frames;

		textureOffset.x = frameNumber * textureStep;
		mat.mainTextureOffset = textureOffset;

		timeElapsed += Time.deltaTime;
	}
}
