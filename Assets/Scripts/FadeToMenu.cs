﻿using UnityEngine;
using System.Collections;

public class FadeToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	public IEnumerator MenuCountdown() {

	
		//float fadeTime = GetComponent<TransitionScene>().BeginFade (1);

		yield return new WaitForSeconds(.1F);	

		SceneFader.Get().FadeToScene("FireflyMenu");


	}

	public void InitiateFade(){
		StartCoroutine(MenuCountdown());
	
	}


	// Update is called once per frame
	void Update () {
	
	}
}
