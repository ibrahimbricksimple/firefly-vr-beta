﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class VideoStateListener : MonoBehaviour {

	public MediaPlayerCtrl mediaPlayer;
	private bool exitScene = false;

	void Start() {
		if(mediaPlayer == null) mediaPlayer = GetComponent<MediaPlayerCtrl>();
	}

	void Update() {
		if(mediaPlayer.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.END && !exitScene) {
			exitScene = true;
			SceneFader.Get().FadeToScene("FireflyMenu");
		}
	}
}
