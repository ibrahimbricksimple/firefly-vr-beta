﻿using UnityEngine;
using System.Collections;

public class CardboardBackButton : MonoBehaviour {
	public bool magnetDetectionEnabled = true;

	void Start() {
		CardboardMagnetSensor.SetEnabled(magnetDetectionEnabled);
		// Disable screen dimming:
		Screen.sleepTimeout = SleepTimeout.NeverSleep;


	} 

	void Update () {
		if (!magnetDetectionEnabled) return;
		if (CardboardMagnetSensor.CheckIfWasClicked()) {
			Debug.Log("DO SOMETHING HERE");  // PERFORM ACTION.
			CardboardMagnetSensor.ResetClick();
		}
	}
}