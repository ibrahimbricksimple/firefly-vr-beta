﻿using UnityEngine;
using System.Collections;

public class FadeLogo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(LogoCountdown());
	}

	public IEnumerator LogoCountdown() {

		// wait until logo animation is complete before fading
		yield return new WaitForSeconds(6.5F);

		Debug.Log ("Fade to scene");
		SceneFader.Get().FadeToScene("FireflyMenu");
		

	}

	// Update is called once per frame
	void Update () {
	
	}
}
