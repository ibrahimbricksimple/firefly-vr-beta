﻿using UnityEngine;
using System.Collections;

public class LoadVideo : MonoBehaviour {

	public GameObject preLoader;

	void Start() {
		
		//GetComponent<MediaPlayerCtrl>().Load(PersistentVideoData.Get().url);
	
		StartCoroutine(InitializeVideo());

	}

	IEnumerator InitializeVideo() {

		// wait 5 seconds before playing video to ensure it initializes properly after Load()
		yield return new WaitForSeconds (5);

		GetComponent<MediaPlayerCtrl> ().Play ();

		StartCoroutine(VideoCountdown());

	}

	IEnumerator VideoCountdown() {

		// if the video is not playing, wait 1 second and check again

		if (GetComponent<MediaPlayerCtrl> ().GetSeekPosition() < 50){

			Debug.Log ("Video not ready");

			yield return new WaitForSeconds (0.5F);

			StartCoroutine(VideoCountdown());

		}else{
			
			Debug.Log ("Video ready");

			// otherwise hide the preloader and bring the video to the stage

			// hide preloader
			preLoader.SetActive (false);

			// add videosphere to stage
			transform.position = new Vector3 (0, 0, 0);

		}
			

	}

}
