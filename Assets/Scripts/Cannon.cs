﻿using UnityEngine;

[RequireComponent(typeof (AudioSource))]
public class Cannon : MonoBehaviour {

	private AudioSource cannonFiringSound;

	[SerializeField] private ParticleSystem particleSystem;
	[SerializeField] private Transform rotatingObject;

	public Vector3 targetRot, startRot;
	private Quaternion targetRotation, startRotation;
	private Quaternion currentTarget;
	[SerializeField] private float rotateDuration;

	private bool gazedAt;
	private float degreesPerSecond;

	void Start() {
		targetRotation = Quaternion.Euler (targetRot);
		startRotation = Quaternion.Euler (startRot);

		degreesPerSecond = Mathf.Abs((Quaternion.Angle(targetRotation, startRotation)) / rotateDuration);
		currentTarget = startRotation;
		rotatingObject.rotation = startRotation;

		cannonFiringSound = GetComponent<AudioSource> ();
	}

	void Update() {
		rotatingObject.rotation = Quaternion.RotateTowards (rotatingObject.rotation, currentTarget, degreesPerSecond * Time.deltaTime);

		if (Input.GetKeyDown ("space"))
			Fire ();

	}

	public void OnGaze(bool gazedAt) {
		this.gazedAt = gazedAt;
		currentTarget = gazedAt ? targetRotation : startRotation;
	}

	public void Fire() {
		particleSystem.Stop();
		particleSystem.Clear();
		particleSystem.Play();
		cannonFiringSound.Play ();
	}
}
