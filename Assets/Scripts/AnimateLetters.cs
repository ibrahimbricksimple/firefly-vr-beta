﻿using UnityEngine;
using System;

public class AnimateLetters : MonoBehaviour {
	public Transform[] array;
	private Transform[] origArray;

	[SerializeField]
	protected bool flattenOnce, flattenAll;

	public AnimationCurve offset;
	public AnimationCurve indivTransX	= AnimationCurve.Linear(0,0,1,1);
	public AnimationCurve indivTransY	= AnimationCurve.Linear(0,0,1,1);
	public AnimationCurve indivTransZ	= AnimationCurve.Linear(0,0,1,1);
	public AnimationCurve indivRotX		= AnimationCurve.Linear(0,0,1,1);
	public AnimationCurve indivRotY		= AnimationCurve.Linear(0,0,1,1);
	public AnimationCurve indivRotZ		= AnimationCurve.Linear(0,0,1,1);
	public AnimationCurve indivRotW		= AnimationCurve.Linear(0,0,1,1);
	public float totalTime;
	[Range(0,1)]
	public float percentComplete;

	void Start(){
		if (origArray == null) {
			origArray = new Transform[array.Length];
			Array.Copy (array, origArray, array.Length);
		}
	}

	void Update(){
		percentComplete += Time.deltaTime / totalTime;
		UpdateTransforms ();
	}


	void OnValidate(){
		if (!flattenOnce && ! flattenAll) 
			return;

		if(flattenOnce)
			array.FlattenOnce (ref array);
		else 
			array.FlattenAll (ref array);
		flattenOnce = false;
		flattenAll = false;
	}

	void UpdateTransforms(){
		for (int i = 0; i < array.Length; i++) {
			array [i].rotation = new Quaternion (indivRotX.Evaluate(percentComplete), indivRotY.Evaluate(percentComplete), indivRotZ.Evaluate(percentComplete), indivRotW.Evaluate(percentComplete));
			array [i].position = origArray[i].localPosition + new Vector3 (indivTransX.Evaluate(percentComplete), indivTransY.Evaluate(percentComplete), indivTransZ.Evaluate(percentComplete));
		}
	}
}
