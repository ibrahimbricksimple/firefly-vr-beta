﻿using UnityEngine;
using System.Collections;

public class popupAnimation : MonoBehaviour {

	public Animator anim;

	private bool gazedAt;

	private float currentLocation;

	public AudioSource audio;

	public int active;

	public int inactive;

	GameObject videoSphere;

	MediaPlayerCtrl videoScript ;

	// Use this for initialization
	 void Start () {

		currentLocation = transform.position.y;
		anim = GetComponentInChildren<Animator>();
		audio = GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		GameObject videoSphere = GameObject.Find("VideoSphere");

		MediaPlayerCtrl videoScript = videoSphere.GetComponent<MediaPlayerCtrl>();

		//MeshRenderer render = gameObject.GetComponentInChildren<MeshRenderer>();

	
		if (videoSphere.GetComponent<MediaPlayerCtrl> ().GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING && videoScript.GetSeekPosition () > active && videoScript.GetSeekPosition () < inactive) {
			
			transform.position = new Vector3(transform.position.x,currentLocation,transform.position.z);

			//Debug.Log("show");

		} else {
			transform.position = new Vector3(transform.position.x,-99999,transform.position.z);
			//Debug.Log("hide");
		}


	}

	public void ShowPopUp(){

		audio.Play ();
		anim.CrossFade ("popup", .2F, -1);

		//Debug.Log(videoScript.GetSeekPosition());

	}

	public void HidePopUp(){

		anim.CrossFade ("popdown",.2F,-1);

	}

	public void OnGaze(bool gazedAt) {
		this.gazedAt = gazedAt;
		//Debug.Log("Gaze");
		//currentTarget = gazedAt ? targetRotation : startRotation;
	}
}

