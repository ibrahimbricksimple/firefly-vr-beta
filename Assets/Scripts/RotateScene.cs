﻿using UnityEngine;
using System.Collections;

public class RotateScene : MonoBehaviour {

	public float rotationAmount;
	public float rotationOffset;

	// Use this for initialization
	void Start () {

		//PersistentVideoData v = new PersistentVideoData ();

		rotationAmount = PersistentVideoData.Get().rotateScene;

		//rotationOffset = PersistentVideoData.Get().rotateMainMenu;

		Debug.Log("Rotation Amount: "+rotationAmount+" / Rotation Offset: "+rotationOffset);

		transform.rotation = Quaternion.Euler(0, rotationAmount+rotationOffset ,0);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
