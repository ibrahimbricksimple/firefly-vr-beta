﻿using UnityEngine;
using System.Collections;

public class PersistentVideoData : MonoBehaviour {

	private static readonly string label = "PersistentVideoData";

	public string url;
	public int rotateScene;

	void Awake() {
		DontDestroyOnLoad(gameObject);

		if(FindObjectsOfType(GetType()).Length > 1) {
			Destroy(gameObject);
		}
	}

	void Start() {
		name = label;
	}

	public static PersistentVideoData Get() {
		GameObject found = GameObject.Find(label);
		if(found != null) return found.GetComponent<PersistentVideoData>();
		return null;
	}
}
