﻿using UnityEngine;

public class LanternSway : MonoBehaviour {

	[Tooltip ("Maximum rotation in degrees. Used for all axis. Keep this small.")]
	public float maxRotation = 0.1F;

	[Tooltip ("The offset from the noise function.")]
	public float offset = 0;

	[Tooltip ("How fast to iterate over the noise function.")]
	public float step = 0.5F;

	private float xSample, zSample;
	private Quaternion initialRotation;
	private Vector3 initialPosition;
	private Vector3 topPoint;

	void Start(){
		initialRotation = transform.rotation;
		initialPosition = transform.position;

		//a trial and error determined value for the top of the lamp cords
		topPoint = transform.position;
		topPoint.y += 27;
	}

	void Update() {
		//increment the sample points
		xSample += step * Time.deltaTime;
		zSample += step * Time.deltaTime;

		//sample the perlin noise and map it to the max rotation
		float xRot = Mathf.PerlinNoise(xSample + offset, 0).Clamp01().Map(0, 1, -maxRotation, maxRotation);
		float zRot = Mathf.PerlinNoise(0, zSample + offset).Clamp01().Map(0, 1, -maxRotation, maxRotation);

		//reset the position and rotation
		transform.rotation = initialRotation;
		transform.position = initialPosition;

		//apply z and x axis rotation
		transform.RotateAround(topPoint, Vector3.forward, zRot);
		transform.RotateAround(topPoint, Vector3.right, xRot);
	}
}