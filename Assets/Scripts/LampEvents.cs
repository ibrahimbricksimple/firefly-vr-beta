﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LampEvents : MonoBehaviour {

	//For adjusting the brightness of the lamp
	[Header("Lamp Lighting")]
	public Light targetLight;
	public float maxIntensity = 8f;
	public float fadeDuration = 2f;
	private float defaultIntensity;
	private float intensityStep;

	//For transitioning to the next scene
	[Header("Scene Transition")]
	public string contentScene = "defaultScene";
	public GameObject transitionPlane;
	public float transitionDuration = 2f;
	public bool overlayWhite;
	private Texture2D transitionSceneTexture;
	private bool isChangingScene = false;
	private float transitionValue = -1;
	private Rect screenRect;
	private float alpha = 0;

	//For the content text below the lantern
	[Header("Content Text")]
	public Canvas canvas;
	public string contentText = "Content Description";
	public float contentTextFadeDuration = 2f;
	public Vector3 contentTextDisplacement = new Vector3 (0, -30f, 0);
	private float contentTextFadeTime;
	private int contentTextFadeDirection;
	private float textFadeStep;
	private Text canvasText;
	private CanvasGroup canvasGroup;


	void Start(){
		//set the original intensity of the light
		defaultIntensity = targetLight.intensity;

		//calculate how quickly the light should change intensity
		float intensityDifference = maxIntensity - defaultIntensity;
		intensityStep = intensityDifference / fadeDuration;

		//make the step negative so it doesn't automatically start fading in
		intensityStep *= -1;

		screenRect = new Rect(0, 0, Screen.width, Screen.height);

		//set the canvas text to what was set in the editor for this script
		canvasText = canvas.GetComponentInChildren<Text>();
		canvasText.text = contentText;

		//start the text to be invisible
		canvasGroup = canvas.GetComponent<CanvasGroup> ();
		canvasGroup.alpha = 0;

		textFadeStep = 1 / contentTextFadeDuration;
		textFadeStep *= -1;

		contentTextFadeDirection = 0;
		contentTextFadeTime = 0;

		transitionSceneTexture = Texture2D.whiteTexture;
	}

	public void OnPointerEnter(){
		//set the step to fade in
		//math.abs helps prevent bugs associated with the user already looking at a sphere
		intensityStep = Mathf.Abs(intensityStep);

		//set the text to fade in
		textFadeStep = Mathf.Abs (textFadeStep);

		//set the text to fall down
		contentTextFadeDirection = 1;
	}

	public void OnPointerExit(){
		//set the step to fade out
		//math.abs helps prevent bugs associated with the user already looking at a sphere
		intensityStep = -1 * Mathf.Abs(intensityStep);

		//set the text to fade out
		textFadeStep = -1 * Mathf.Abs(textFadeStep);

		//set the text to move up
		contentTextFadeDirection = -1;
	}

	public void OnPointerClick(){

		// set scene rotation so next scene is oriented towards Cardboard camera
		GameObject.Find ("PersistentVideoData").GetComponent<PersistentVideoData> ().rotateScene = (int)GameObject.Find ("Main Camera").transform.eulerAngles.y;

		if (!isChangingScene) {
			isChangingScene = true;
			transitionValue = 0;
		}
	}

	void Update(){
		//update the light intensity but don't let it get beyond the start value or the max value
		targetLight.intensity = Mathf.Clamp (targetLight.intensity + intensityStep * Time.deltaTime, defaultIntensity, maxIntensity);

		//update the text alpha but clamp its value
		canvasGroup.alpha = Mathf.Clamp01(canvasGroup.alpha + textFadeStep * Time.deltaTime);

		//move the text
		canvasText.rectTransform.anchoredPosition3D = new Vector3(
			(float)Easing.CubicEaseOut (contentTextFadeTime, 0, contentTextDisplacement.x, contentTextFadeDuration),
			(float)Easing.CubicEaseOut (contentTextFadeTime, 0, contentTextDisplacement.y, contentTextFadeDuration),
			(float)Easing.CubicEaseOut (contentTextFadeTime, 0, contentTextDisplacement.z, contentTextFadeDuration));
		
		//update the time in the text movement ease
		if(contentTextFadeDirection != 0)
			contentTextFadeTime += contentTextFadeDirection * Time.deltaTime;

		//stop moving the text if it is at its highest or lowest point
		if (contentTextFadeTime <= 0 || contentTextFadeTime >= contentTextFadeDuration)
			contentTextFadeDirection = 0;


		if (!(transitionValue < 0)) {
			//update the transition progress and change the overlay size based on that value
			transitionValue += Time.deltaTime / transitionDuration;
			transitionPlane.transform.localScale = new Vector3 (transitionValue, transitionValue, transitionValue);

			//check to see if the content scene should be loaded
			if(transitionValue >= 1)
				SceneManager.LoadScene (contentScene);
		}
	}

	void OnGUI(){
		if (!(transitionValue < 0) && overlayWhite) {

			//gradually overlay the white texture on the camera to fade the rest of the scene out
			alpha += Time.deltaTime / transitionDuration;
			GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
			GUI.depth = -1000; // Draw in front of everything else
			GUI.DrawTexture(screenRect, transitionSceneTexture);
		}
	}
}