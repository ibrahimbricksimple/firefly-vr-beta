﻿using UnityEngine;
using System.Collections;


public class VideoPreloader : MonoBehaviour {

	public bool LoadTimeComplete = false;

	// Use this for initialization
	void Start() {
		Debug.Log ("Initialize Preloader");
		StartCoroutine(VideoCountdown());
	}

	IEnumerator VideoCountdown() {

		GetComponent<MediaPlayerCtrl> ().SetVolume (0.0F);

		StartCoroutine(VideoLoader());

		yield return new WaitForSeconds(10);

		// flag after video has attempted to load for 10 secs
		LoadTimeComplete = true;

		Debug.Log ("Play Movie: " + GetComponent<MediaPlayerCtrl>().GetCurrentSeekPercent ());


	}

	IEnumerator VideoLoader() {



		if (GameObject.Find ("VideoSphere").GetComponent<MediaPlayerCtrl> ().GetCurrentSeekPercent() < 70 && !LoadTimeComplete) {

			Debug.Log ("Video not started");

			yield return new WaitForSeconds(0.05F);	

			StartCoroutine(VideoLoader());
		
		} else {
			
			// if the video is over 70% loaded (android only) or if it has been 10 seconds -- play the video

			Debug.Log ("Video started: "+GetComponent<MediaPlayerCtrl> ().GetSeekPosition ());

			GetComponent<MediaPlayerCtrl> ().Play();

			// wait until video player has started playing before moving to main stage
			StartCoroutine(WaitToStart ());

			// remove preloader from stage
			GameObject.Find("Preloader").transform.position = new Vector3(0, 10, 0);

			// add videosphere to stage
			transform.position = new Vector3(0, 0, 0);

			GetComponent<MediaPlayerCtrl> ().SetVolume (1.0F);
		
		}
			
	}

	IEnumerator WaitToStart() {
		
		if (GetComponent<MediaPlayerCtrl>().GetSeekPosition () < 5) {

			Debug.Log ("Video seek position: "+GetComponent<MediaPlayerCtrl> ().GetSeekPosition ());

			yield return new WaitForSeconds (0.5F);	

			StartCoroutine ( WaitToStart());
		}

	}

	// Update is called once per frame
	void Update () {
		//Debug.Log (GetComponent<MediaPlayerCtrl>().GetSeekPosition()+" / " + GetComponent<MediaPlayerCtrl>().GetDuration());
	}
}
